FROM php:7.3-fpm-alpine

RUN apk add --no-cache freetype-dev libpng-dev libzip-dev \
  && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ \
  && docker-php-ext-install gd mysqli

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"